package sg.ncs.product.0hf034hf3f.common.feign;

import feign.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignLoggerFactory;

public class InfoFeignLoggerFactory implements FeignLoggerFactory {

@Override
    public Logger create(Class<?> type) {
        return new InfoFeignLogger(LoggerFactory.getLogger(type));
    }
}
